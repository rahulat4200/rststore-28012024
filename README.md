cd rststore
cd frontend
[DELETE .git folder and .gitignore file]
cd ..
[COPY my .gitignore file]
git config --global user.name "YOUR NAME"
git config --global user.email "YOUR EMAIL"
git init
git add .
git commit -m "Ready for deployment"
git remote add origin https://gitlab.com/YOUR_USERNAME/YOUR_REPO.git
git push -u origin master

## LINODE
ssh root@192.46.208.36
yes
[ENTER PASSWORD]

- Install NODEJS
sudo apt update
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
source ~/.bashrc
nvm install 20
node -v
npm -v

# Install MONGODB
sudo apt-get install gnupg curl
curl -fsSL https://www.mongodb.org/static/pgp/server-7.0.asc | \
   sudo gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg \
   --dearmor
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/7.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-7.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo systemctl start mongod
sudo systemctl status mongod
sudo systemctl enable mongod

# Install PM2
npm install pm2 -g

# Git Clone
git clone https://gitlab.com/YOUR_USERNAME/YOUR_REPO.git
ls
cd rststore-28012024/
cd frontend/
npm install
npm run build
cd ..
npm install
touch .env
nano .env
#	[PASTE YOUR .env CONTENT, Ctrl o, ENTER, Ctrl x]
cat .env

# Start application
pm2 start backend/server.js
# [EDIT THE PORT NUMBER TO BE 80]
nano .env
#	[EDIT, Ctrl o, ENTER, Ctrl x]
pm2 restart 0



# [CHANGE LOCALLY]
git add .
git commit -m "YOUR MESSAGE"
git push

# [ON THE SERVER]
ssh root@192.46.208.36
cd rststore-28012024/
git pull
cd frontend/
npm run build
pm2 restart 0



# pm2 stop 0
# pm2 logs 0
